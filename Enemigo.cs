﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Enemigo : Entidad
    {
        public string nombre = "Tsubasa";
        public Enemigo(int ValorAtaque, int Mana, int vida, int ValorDefensa)
        {
            this.ValorAtaque1 = ValorAtaque;
            this.ValorDefensa1 = ValorDefensa;
            this.Vida = vida;
            this.Mana = Mana;
        }
        public string Atacar()
        {
            return ("Ahora conoceras mi verdadera fuerza");
        }
        public string mover()
        {
            return ("El enemigo se ha movido");
        }
        public string saltar()
        {
            return ("El enemigo decidio saltar");
        }
        public string defender()
        {
            return ("Jajaja, a eso le llamas ataque");
        }
        public string muerte()
        {
            return ("Wasted");
        }
        public void estadisticas()
        {
            Console.WriteLine("Estadisticas del enemigo");
            Console.WriteLine("Nombre: " + nombre);
            Console.WriteLine("Puntos de vida: " + Vida + "PV");
            Console.WriteLine("Mana: " + Mana);
            Console.WriteLine("Valor de ataque: " + ValorAtaque1);
            Console.WriteLine("Valor de defensa: " + ValorDefensa1);
        }
    }
}