﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    public class Jugador : Entidad
    {
        public string nombre;
        public int opcion;
        public Jugador(int ValorAtaque, int Mana, int vida, int ValorDefensa)
        {
            this.ValorAtaque1 = ValorAtaque;
            this.ValorDefensa1 = ValorDefensa;
            this.Vida = vida;
            this.Mana = Mana;
        }
        public void Atacar()
        {
            Console.WriteLine("Toma esto!!!");
        }
        public void mover()
        {
            Console.WriteLine("Prefiero evitar por el momento el enfrentamiento");
        }
        public void saltar()
        {
            Console.WriteLine("Salto");
        }
        public void defender()
        {
           Console.WriteLine("Bloqueare tu ataque auque me cueste ambos brazos!!!");
        }
        public void muerte()
        {
            Console.WriteLine("Wasted");
        }
        public void nombres()
        {
            Console.WriteLine("Escribe el nombre de tu jugador");
            nombre = Convert.ToString(Console.ReadLine());
        }
        public void estadisticas()
        {
            Console.WriteLine("Estadisticas del jugador");
            Console.WriteLine("Nombre: "+ nombre);
            Console.WriteLine("Puntos de vida: " + Vida+ "PV");
            Console.WriteLine("Mana: " + Mana);
            Console.WriteLine("Valor de ataque: " + ValorAtaque1);
            Console.WriteLine("Valor de defensa: "+ ValorDefensa1);
        }
        public void menu()
        {
            Console.WriteLine("Elige una accion");
            Console.WriteLine("1) Atacar");
            Console.WriteLine("2) Defender");
            Console.WriteLine("3) Saltar");
            Console.WriteLine("4) Moverse");
            opcion = Convert.ToInt16(Console.ReadLine());
          
                switch (opcion)
                {
                    case 1:
                        Console.Clear();
                        opcion = 1;
                        Atacar();
                        Console.WriteLine(nombre + " " + "Realizo " + ValorAtaque1 + " " + "de daño al enemigo");
                        break;

                    case 2:
                        opcion = 2;
                        defender();
                        break;

                    case 3:
                        opcion = 3;
                        saltar();
                        break;

                    case 4:
                        opcion = 4;
                        mover();
                        break;

                    default:
                        Console.WriteLine("Opcion invalida");
                        break;
                }
            
            }
            }
    }

